module.exports = function(dimensions) {
  
  return {

	  container: {
	    flex: 1,
	  },

	  logoContainer: {
	    height: dimensions.height / 3,
	    flex: 1,
	    alignItems: 'center',
	    justifyContent: 'center',
	  },

	  formContainer: {
	    height: dimensions.height / 3 * 2,
	  },

	  logo: {
	    marginTop: 200,
	    width: 250,
	    alignItems: 'center',
	    justifyContent:'center',
	  }

	}

}
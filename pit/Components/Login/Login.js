import React from 'react'
import { Text, View, ImageBackground, Image, StyleSheet, Dimensions } from 'react-native'
import CodeInput from 'react-native-confirmation-code-input'

const dimensions = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height
}

export default class Login extends React.Component {

  constructor(props) {
    super(props)
    this.initComponent = this.initComponent.bind(this)
    this.isCodeValid = this.isCodeValid.bind(this)
    this.initComponent()
  }

  initComponent() {

  }

  isCodeValid(res) {
    console.log(res)
  }

  render() {
    return (
      <ImageBackground source={require('./../../img/1.png')} style={styles.container}>
        <View style={styles.logoContainer}>
          <Image resizeMode="contain" style={styles.logo} source={require('./../../img/logo.png')} />
        </View>
        <View style={styles.formContainer}>
        </View>
        <CodeInput ref="codeInputRef2" keyboardType="numeric" codeLength={4} secureTextEntry compareWithCode='1234' activeColor='rgba(49, 180, 4, 1)' inactiveColor='rgba(49, 180, 4, 1.3)' autoFocus={false} ignoreCase={true} inputPosition='center' size={50} onFulfill={(isValid) => this.isCodeValid(isValid)} containerStyle={{ marginTop: 0 , position: 'relative' , bottom: 200 , }} codeInputStyle={{ borderWidth: 1.5 }} />
      </ImageBackground>
    )
  }
}

const styles = StyleSheet.create({

  container: {
    flex: 1,
  },

  logoContainer: {
    height: dimensions.height / 3,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },

  formContainer: {
    height: dimensions.height / 3 * 2,
  },

  logo: {
    marginTop: 200,
    width: 250,
    alignItems: 'center',
    justifyContent:'center',
  }

})
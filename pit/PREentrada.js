import React from 'react';
import { StyleSheet, Text, View, ImageBackground, Image } from 'react-native';


export default class App extends React.Component {

  render() {
    return (
      <ImageBackground source={require('./img/1.png')} style={styles.container}>
      <View style={styles.logoContainer}>
        <Image resizeMode="contain" style={styles.logo} source={require('./img/logo.png')} />
      </View>
      <View>
        <Text style={styles.perfil}>PERFIL</Text>
      <View>
        <Image style={styles.icoprofile} source={require('./img/profile.png')}/>
      </View>
      </View>
      <View>
        <Text style={styles.planos}>PLANOS DE TREINO</Text>
      <View>
        <Image style={styles.icoplan} source={require('./img/plan.png')}/>
      </View>
      </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },

  logo: {
    width: 150,
    alignItems: 'center',
    justifyContent:'center',
  },

  icoprofile: {
    width: 45,
    height: 45,
    position: 'relative',
    top: -53,
    left: 10,
  },

  icoplan: {
    width: 45,
    height: 45,
    position: 'relative',
    top: -52,
    left: 10,
  },

  perfil: {
    marginTop: 90,
    borderRadius: 4,
    width: 300,
    paddingTop: 20,
    paddingBottom: 20,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(255, 255, 255, 0.5)',
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center',
  },

  planos: {
    marginTop: 10,
    borderRadius: 4,
    width: 300,
    paddingTop: 20,
    paddingBottom: 20,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(255, 255, 255, 0.5)',
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center',
  },

});


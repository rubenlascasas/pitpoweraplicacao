class User {

	constructor(data) {
		this.name = data.name
		this.birthDate = data.birthDate
		this.height = data.height
		this.evaluations = []
	}

	addEvaluation(evaluation) {
		this.evaluations.push(evaluation)
	}

}

module.exports = function(data) {
	return new User(data)
}

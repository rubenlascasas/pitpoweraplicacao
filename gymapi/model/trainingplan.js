class TrainingPlan {

	constructor(data) {
		this.trainingPlan = []
		if (data.trainingPlan !== null && data.trainingPlan !== undefined) 
			this.trainingPlan = data.trainingPlan
	}
	
}

module.exports = function(data) {
	return new TrainingPlan(data)
}

class TrainingPhase {

	constructor(data) {
		this.name = data.name
		this.exerciseList = []
		if (data.exerciseList !== null && data.exerciseList !== undefined) 
			this.exerciseList = data.exerciseList
	}

	addExercise(exercise) {
		this.exerciseList.push(exercise)
	}
	
}

module.exports = function(data) {
	return new TrainingPhase(data)
}

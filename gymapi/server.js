
var express = require('express')
var bodyParser = require('body-parser')
var https = require('https')
var http = require('http')
var cors = require('cors')
//const https = require('https')
const fs = require('fs')
const path = require('path')
const HTTP_PORT = 3100

var server = express()
server.use(cors())
server.use(bodyParser.json({limit: '5mb'}))
server.use(bodyParser.urlencoded({limit: '5mb'}))

server.use('/auth', require('./services/authenticationservices'))
server.use('/exercise', require('./services/exerciseservices'))

server.get('/', function(req, res) {
	res.send('gymapi is online')
})

var insecureServer = http.createServer(server).listen(HTTP_PORT, function() {
  console.log('HTTP IS RUNNING ON PORT: ' + HTTP_PORT)
})
const express = require('express')
const assert = require('assert')
var auth = express.Router()

const mongoConn = require('./../mongoconn/mongoconn')

auth.post('/createUser', function(req, res) {
	mongoConn(function(err, client) {
  		assert.equal(null, err)
		const db = client.db('gym')
		const collection = db.collection('user')
		collection.insertMany([
			req.body.user
		], function(err, result) {
		assert.equal(err, null)
			res.send({result: 'user has been created'})
		})
		client.close()
	})
})

auth.post('/login', function(req, res) {
	mongoConn(function(err, client) {
  		assert.equal(null, err)
		const db = client.db('gym')
		let col = db.collection("user")
		col.find({phoneNumber: req.body.phoneNumber, keyCode: req.body.keyCode}).toArray(function(err, docs) {
		assert.equal(err, null)
			res.send(docs[0])
		})
		client.close()
	})
})

module.exports = auth
const express = require('express')
const assert = require('assert')
var exer = express.Router()

const mongoConn = require('./../mongoconn/mongoconn')

exer.get('/findallexercises', function(req, res) {
	mongoConn(function(err, client) {
  		assert.equal(null, err)
		const db = client.db('gym')
		const collection = db.collection('exercise')
		collection.find({}).toArray(function(err, docs) {
			assert.equal(err, null)
			res.send(docs)
		})
		client.close()
	})
})

exer.get('/findexercise', function(req, res) {
	mongoConn(function(err, client) {
  		assert.equal(null, err)
		const db = client.db('gym')
		const collection = db.collection('exercise')
		console.log(req.query)
		collection.find({number: parseInt(req.query.number, 10)}).toArray(function(err, docs) {
			assert.equal(err, null)
			res.send(docs[0])
		})
		client.close()
	})
})

module.exports = exer
const MongoClient = require('mongodb').MongoClient
const url = 'mongodb://localhost:27017'
const dbName = 'gym'

module.exports = function(callback) {
	MongoClient.connect(url, callback)
}

/*

 
MongoClient.connect(url, function(err, client) {
  assert.equal(null, err)
  console.log("Connected successfully to server")
  const db = client.db(dbName)
  let col = db.collection("user")
  col.find({}).toArray(function(err, docs) {
    assert.equal(err, null)
    console.log("Found the following records")
    console.log(docs)
  })
  client.close()
})

*/